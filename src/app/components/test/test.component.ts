import { Component, OnInit } from '@angular/core';
import { TestService } from 'src/app/service/test.service';
import {NgbModal, ModalDismissReasons, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

//Variable para iniciar la paginacion
  p: number = 1;
  //Array donde se guarda la respuesta de la API
  data : any = [];
  //Array donde se guardar el nuevo registro
  registro : any = [];

  //Se define el servicio para consumir la API, se define el servicio del modal
  constructor(private testService: TestService,private modalService: NgbModal ) { }

  ngOnInit() {
    //Consumir API 
    this.testService.consumirApi()
    .subscribe(
      (res) => { 
        //Respuesta API
        this.data = res;
        
      },
      (err) => {
        //Si se produce un error
        console.error(err);
      }
    );
  }

  //Metodo para guardar un nuevo registro
  guardarRegistro(){
    //Agregar el registro al inicio de nuestro array
    this.data.unshift(this.registro);
    //Vaciar el registro insertado
    this.registro=[];
    //Producir una alerta de guardado
    Swal.fire("Hecho!", "Se guardó correctamente!", "success");
  }

  eliminarRegistro(indice){
    //Eliminar el registro seleccionado
    this.data.splice(indice, 1);
  }
//Metodo para abrir un modal
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
  }


}
