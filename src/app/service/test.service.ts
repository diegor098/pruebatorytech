import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(private http: HttpClient) { }

  consumirApi(){
   return this.http.get("https://jsonplaceholder.typicode.com/posts");
  }
}
